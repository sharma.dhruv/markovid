# Macroeconomic Shocks to Mark0 

## Quick Start 

For compiling and running the code on the command line, simply do

```shell
make markovid
mkdir output
./covid 
```

This will run the basic Mark0 model without any shocks with the results stored in a text file in the folder `output`. 

The program takes a certain number of parameters as input. You can specify them as follows (for instance to change the maximum credit supply available to firms):

``` shell
./markovid --theta=2.0
```

Multiple options can be changed as follows:

``` shell
./markovid --theta=2.0 --nfirms=5000
```

The complete list of command-line options are: 

```console
./markovid [OPTION...]

    --R             Ratio of hiring-firing rate (default: 2.0)
    --theta         Maximum credit supply available to firms (default: 3.0)
    --Gamma0        Financial fragility sensitivity (default: 0.0)
    --rho0          Baseline interest rate (default: 0.0)
    --alpha         Influence of deposit rates on consumption (default: 0.0)
    --phi_pi        Intensity of interest rate policy of central bank (default: 0.0)
    --alpha_e       Influence of employment on interest rate policy of central bank (default: 0.0)
    --pi_star       Inflation Target (default: 0.0)
    --e_star        Employment Target (default: 0.0)
    --tau_tar       Inflation target parameter (default: 0.0)
    --wage_factor   Factor to adjust wages to inflation expectations (default: 1.0)
    --y0            Initial production (default: 0.5)
    --gammap        Parameter to set adjustment of prices (default: 0.01)
    --eta0m         Firing propensity (default: 0.2)
    --tau_r         Realised inflation parameter (default: 0.0)
    --alpha_g       Influence of loans interest rate on hiring-firing policy (default: 0.0)
    --seed          seed for random number generation (default: 0)
    --shockflag     Flag to set kind of shock (default: 0)
    --t_start       Time when shock occurs (default: 2000)
    --t_end         Time when shock end (default: 2005)
    --policy_start  Time when debt policy starts (default: 2000)
    --policy_end    Time when debt policy ends (default: 2005)
    --output        User specified output file name.
    --cfactor       Factor by which to reduce consumption during shock (default: 0.5)
    --zeta          Labor productivity factor (default: 1.0)
    --zfactor       Factor by which to reduce productivity during shock (default: 0.5)
    --kappa         Offset for adaptive policy (default: 1.25)
    --helico        Do helicopter money drop (default: 0.0)
    --nfirms        Number of firms in the economy (default: 10000)
    --extra_cons    Agents consume more after shock (default: 0.0)
    --adapt         Have adaptive policy (default: 0.0)
    --extra_steps   Number of time steps during which consumption is higher than baseline (default: 1.0)
    --tsim          Simulation length (default: 7000)
    --teq           Equilibration time (default: 200)
    --tprint        Frequency to write to output (default: 1)
    --G0            Fraction of savings to consume (default: 0.5)
    --phi           Revival probability per unit time (default: 0.1)
    --omega         Parameter for moving average (default: 0.2)
    --delta         Fraction of dividends to be distributed (default: 0.02)
    --renorm        Renormalize in units of price
    --help          Print usage
```


Shocks can be set setting the parameter `shockflag`. The following shocks are accepted: 

1. `--shockflag=0`: No shock (default)
2. `--shockflag=1`: Pure Consumption shock 
3. `--shockflag=2`: Consumption + Production shock
4. `--shockflag=3`: Consumption Shock + Naive Debt policy
5. `--shockflag=4`: Consumption Shock + Production Shock + Naive Debt policy


## Linking 

No linking is required. All dependencies are header only. 

## Building 

### Without CMake

Without CMake, simply using the basic makefile should suffice: 

``` shell
make markovid
./markovid
```

### With CMake

If you have CMake installed, a `CMakeLists.txt` is also provided. To build and run, do:

``` shell
mkdir build
cd build
cmake ..
make markovid 
```
**Note**: Create a folder `output` in the directory `build` before running the compiled program. 

## Using Python notebooks

To facilitate easy exploration, we also provide a python script `notebook_utils.py` and a notebook `example_notebook.ipynb`. All command line options can be set from within the notebooks. The `notebook_utils.py` script also has some functions to easily plot and visualize the results of the simulation. 

## Requirements

### C++

For compiling the Mark0 model with shocks, a std-c++11 compatible compiler is enough. So g++ or gcc >= 4.8 or clang >= 3.1 are known to work.  

The code has been tested on Linux and MacOSX. 

### Python 

For running the scripts and notebooks, a certain number of libraries need to be installed. These are specified in `requirements.txt` file. 

Using Python-3.7 or higher is ideal, but the scripts run for Python-3.5 as well. 

Python-2.7 is not supported at the moment. 

## Issues/Problems 

For any code related issue, please open an issue here or send an email to either: 

1. Dhruv Sharma (dhruv dot sharma at phys dot ens dot fr)
2. Stanislao Gualdi (stanislao dot gualdi at cfm dot fr)
