import pandas as pd
import os
import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.style.use('seaborn-ticks')
import sys
import numpy as np
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

PATH_TO_MARKOVID =os.getcwd()

output_colnames = ['t',
             'u',
             'bust',
             'Pavg',
             'Wavg',
             'S',
             'Atot',
             'firm-savings',
             'debt-tot',
             'inflation',
             'pi-avg',
             'propensity',
             'k',
             'Dtot',
             'rhol',
             'rho',
             'rhod',
             'pi-used',
             'tau-tar',
             'tau-meas',
             'R', 
            'Wtot',
            'etap-avg',
            'etam-avg', 
            'w-nominal',
            'Ytot',
             'deftot', 
            'profits',
            'debt-ratio',
            'firms-alive',
            'left',
            'u-af', 
            'bust-af', 
            'frag', 
            'true_end',
            'theta', 
             'ytot-temp', 
             'min-ytot']
            


flagnames = ['base','cons_pure', 'cons_prod','cons_theta', 'prod_debt']
fnames = [os.path.join('output', flagname) for flagname in flagnames]
    
def read_output(shockflag, res_dir=PATH_TO_MARKOVID):
    '''
    Given a shockflag, reads the output of the simulation.
    Parameters:
    shockflag (int): Shockflag

    Returns:
    pandas.DataFrame: Returns the output as a pandas dataframe. 
    '''
    fname = fnames[shockflag]
    res = {}
    fname = os.path.join(res_dir, fname+'.txt')
    tmp = pd.read_csv(fname, sep = "\t", header = None, index_col = False)
    assert len(tmp.columns) == len(output_colnames)
    tmp.columns = output_colnames
    return tmp

def generate_param_list(param_names, param_values, base_string='./markovid'):
    '''
    Prepares the command line string with parameters to run program from command line. 

    Parameters:
    param_names  (list): Names of parameters that will be passed
    param_values (list): Values of the named parameters
    base_string  (str): Name of program to run (default: markovid)
    
    Returns:
    program_string (str): Program with command line parameters. 
    '''
    for param_name, param_value in zip(param_names, param_values):
        #change to format to ensure python<3.7 compatibility
        base_string += " --{}={} ".format(param_name, param_value)
        
#         base_string += f" --{param_name}={param_value} "
    return base_string


def run_program_default(param_names, param_values, program_name = './markovid'):
    '''
    Runs the markovid program on the command line. If the program is not found, 
    compiles it first and then runs it 
    
    Parameters:
    param_names  (list): Names of parameters that will be passed
    param_values (list): Values of the named parameters
    program_name  (str): Name of program to run (default: markovid)
    
    Returns:
    program_output: Return value from call to os.system
    '''
    program_string = generate_param_list(param_names, param_values, program_name)
    if (not os.path.isfile('markovid')):
        print("Compiling program")
        os.system('make markovid')
    if (not os.path.isdir('output')):
        print("Creating output folder")
        os.makedirs('output')
    print("Running program")
    print(program_string)
    program_output = os.system(program_string)
    return program_output

def plot_interest_rates(res, t_start, t_end, plot_end = 2120, is_shade = True):
    
    start=t_start-10
    end = plot_end
    y = range(start-t_start, end-t_start)
    f, ax = plt.subplots(nrows = 1, ncols =3, figsize=(8, 2), dpi=200,)
    cols1 = ['rho', 'rhol', 'rhod']
    colnames1 = ['$\\rho$', '$\\rho^l$', '$\\rho^d$']

    for i in range(3):
        ax[i].plot(y,res[cols1[i]][start:end])
        ax[i].set_title(colnames1[i])
#     ax[0,-1].plot(y, 1+(res['Pavg'][start:end]-1)*12, label = "1+ Year. Infl.")
#     ax[0,-1].plot(y, res[cols1[-1]][start:end], label = "Real Wages")
#     ax[0,-1].legend()
#     ax[0,-1].set_title(colnames1[-1])
#     for i in range(3):
#         ax[1,i].plot(y, res[cols2[i]][start:end])
#         ax[1,i].set_title(colnames2[i])
    plt.tight_layout()
    
#     axins2 = inset_axes(ax[1,-1], width="50%", height="40%", loc=1, borderpad=1.5)
#     axins2.plot(y,np.minimum(res[col_inset[2]][start:end],20.), label = "$\\rho^l$", ls = '-.')
#     axins2.plot(y,res[col_inset[-1]][start:end].abs(), label = "$\\rho^d$", lw = 0.9)
#     axins2.set_title('Interest rates')
#     axins2.set_xticklabels('')
#     axins2.legend(prop={'size': 12})
#     axins2.axvspan(0, t_end-2000, facecolor='0.5', alpha=0.5)
#     axins2.set_ylim(bottom = -0.1,top = 2.1)
    
    if is_shade:
        for axis in ax.flatten():
            axis.axvspan(0,t_end-t_start, facecolor='0.5', alpha=0.5)
    plt.suptitle("Interest rates", fontsize=12)
    plt.subplots_adjust(top=0.8)
    

            
def plot_evolution(res, t_start, t_end, plot_end = 2120, is_shade = True):
    '''
    Plots the evolution of the economy after the shock
    
    Parameters:
    res (pandas.DataFrame): DataFrame with output from simulation
    t_start (int): Start of the shock 
    t_end (int): End of the shock
    
    Returns
    Generates the plot inline. 
    '''
    col_inset = ['u', 'bust-af', 'rhol', 'rhod']

    start=t_start-10
    end = plot_end
    y = range(start-t_start, end-t_start)
    f, ax = plt.subplots(nrows = 2, ncols =3, figsize=(12, 4), dpi=200,)
    cols1 = ['min-ytot', 'u', 'Pavg', 'Wavg']
    cols2 = ['bust-af', 'frag', 'S']
    colnames1 = ['Total output', 'U-rate', 'Inflation/Wages']
    colnames2 = ['Bankruptcies', 'Fragility', 'Savings']

    for i in range(2):
        ax[0,i].plot(y,res[cols1[i]][start:end])
        ax[0,i].set_title(colnames1[i])
    ax[0,-1].plot(y, 1+(res['Pavg'][start:end]-1)*12, label = "1+ Year. Infl.")
    ax[0,-1].plot(y, res[cols1[-1]][start:end], label = "Real Wages")
    ax[0,-1].legend()
    ax[0,-1].set_title(colnames1[-1])
    for i in range(3):
        ax[1,i].plot(y, res[cols2[i]][start:end])
        ax[1,i].set_title(colnames2[i])
    plt.tight_layout()
    
#     axins2 = inset_axes(ax[1,-1], width="50%", height="40%", loc=1, borderpad=1.5)
#     axins2.plot(y,np.minimum(res[col_inset[2]][start:end],20.), label = "$\\rho^l$", ls = '-.')
#     axins2.plot(y,res[col_inset[-1]][start:end].abs(), label = "$\\rho^d$", lw = 0.9)
#     axins2.set_title('Interest rates')
#     axins2.set_xticklabels('')
#     axins2.legend(prop={'size': 12})
#     axins2.axvspan(0, t_end-2000, facecolor='0.5', alpha=0.5)
#     axins2.set_ylim(bottom = -0.1,top = 2.1)
    
    if is_shade:
        for axis in ax.flatten():
            axis.axvspan(0,t_end-t_start, facecolor='0.5', alpha=0.5)
    plt.suptitle("Evolution after shock", fontsize=16)
    plt.subplots_adjust(top=0.85)
    

    
    